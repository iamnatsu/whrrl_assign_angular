import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpserviceService {
  url = 'http://localhost:8000';

  constructor(public http: HttpClient) {}

  login(data: object): Observable<any> {

    return this.http.post(this.url + '/login', data);
  }
  
  register(data: object): Observable<any> {

    return this.http.post(this.url + '/signin', data);
  }

  create(data: object): Observable<any> {

    return this.http.post(this.url + '/createUser', data);
  }

  getUser(){
    return this.http.get(this.url + '/getUser');
  }

  getCSV(){
    return this.http.get(this.url + '/getCSV');
  }
  
}
