import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {HttpserviceService} from "../httpservice.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public logindata = {};
  public message: any;
  constructor(private router: Router,public data: HttpserviceService) { 
    this.logindata = { username: '', password: ''};
  }
   
  ngOnInit() {
  }
   
  login(){
   this.data.login(this.logindata).subscribe(
    (data: any) => {
      if (data.success === true) {
        this.router.navigateByUrl('/dashboard');
        this.message = 'success';
      }
      if (data.success === false) {
        this.message = 'Incorrect Username or Password';
      }
    },
    (error: any) => {
      console.log('Error', error);
      if (error.error) {
        this.message = 'Incorrect Username or Password';
      }
    }
   )
  }

  registration(){
    this.router.navigateByUrl('/register');
  }

}
