import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {HttpserviceService} from "../httpservice.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public data = {};
  public message: any;
  constructor(private router: Router,public http: HttpserviceService) {
    this.data = {
      name:'',
      email:'',
      mobile_no:'',
      gender:'',
      photo_url:''
    };
   }

  ngOnInit() {

  }

  create(){
    console.log("----------",this.data)
    this.http.create(this.data).subscribe(
      (data: any) => {

        if (data.success === true) {
          this.message = 'success';
        }
        if (data.success === false) {
          this.message = 'All Fileds Required';
        }
      },
      (error: any) => {
        console.log('Error', error);
        if (error.error) {
          this.message = 'All Fileds Required';
        }
      }
     )
  }

  reports(){
    this.router.navigateByUrl('/reports');
  }

}
