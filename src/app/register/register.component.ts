import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {HttpserviceService} from "../httpservice.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public data = {};
  public message: any;

  constructor(private router: Router,public http: HttpserviceService) {
    this.data = { username: '', password: ''};
   }

  ngOnInit() {
  }

  signin(){
    this.http.register(this.data).subscribe(
      (data: any) => {
        if (data.success === true) {
          this.router.navigateByUrl('/login');
          this.message = 'success';
        }
        if (data.success === false) {
          this.message = 'Username Already Exits';
        }
      },
      (error: any) => {
        console.log('Error', error);
        if (error.error) {
          this.message = 'Username Already Exits';
        }
      }
     )
  }

  loginRoute(){
    this.router.navigateByUrl('/login');
  }
}
