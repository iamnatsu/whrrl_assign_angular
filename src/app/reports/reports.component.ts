import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {HttpserviceService} from "../httpservice.service";

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  public data = {};
  constructor(private router: Router,public http: HttpserviceService) {
    this.data={};
   }

  ngOnInit() {
    this.http.getUser().subscribe(
      (result => {
        this.data = result;
      })
     )
  }
  
  DownloadCSV(){
    this.http.getCSV().subscribe(
      (result => {
        this.data = result;
      })
     )
  }
}
